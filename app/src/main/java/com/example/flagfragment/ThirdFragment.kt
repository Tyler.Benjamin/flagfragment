package com.example.flagfragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.width
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.platform.ViewCompositionStrategy
import androidx.compose.ui.unit.dp
import androidx.fragment.app.Fragment
import com.example.flagfragment.ui.theme.FlagFragmentTheme

class ThirdFragment : Fragment() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        return ComposeView(requireContext()).apply {
            setViewCompositionStrategy(ViewCompositionStrategy.DisposeOnViewTreeLifecycleDestroyed)
            setContent {
                FlagFragmentTheme {
                    androidx.compose.material3.Surface(
                        modifier = Modifier.fillMaxSize(),
                        color = MaterialTheme.colorScheme.background
                    ) {

                    }
                    // to nav fwd
                    // activity to activity -> Intent(this, ActivitiytoGoTo::class.java
                    // Fragment to Frag (with navigation component) -> findNAvcontroller().navigate(R.id.destination)

                    // to go back and finish()
                    //Activity -> finish()
                    //Fragment (with Nav COmponent) findNavController().navigateUp
                    Text(text = "Hello world.")
                }
            }
        }
    }
}

@Composable
fun Vertical1(){
    androidx.compose.material3.Surface(modifier = Modifier.width(150.dp), color = Color.Black) {

    }
}

